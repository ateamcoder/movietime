package com.ateam.movietime.connection

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ateam.movietime.MainActivity
import com.ateam.movietime.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser
        if (user != null) {
            if(user.isEmailVerified) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }

        button_login.setOnClickListener {
            preformLogin(auth)
        }

        dont_have_account_textview.setOnClickListener {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }

        reset_password.setOnClickListener {
            val intent = Intent(this, PasswordResetActivity::class.java)
            startActivity(intent)
        }
    }

    private fun preformLogin(auth: FirebaseAuth) {
        val emailText = email.text.toString()
        val passwordText = password.text.toString()

        if(emailText.isEmpty()){
            email.error = "The email cannot be empty"
            return
        }

        if(passwordText.isEmpty()){
            password.error = "The password cannot be empty"
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()){
            email.error = "This email address is not valid"
            return
        }

        Log.d("Login", "attempt login with email and password")
        auth.signInWithEmailAndPassword(emailText, passwordText)
            .addOnCompleteListener{
                val user = auth.currentUser
                if (it.isSuccessful) {
                    if (user != null) {
                        if (user.isEmailVerified) {
                            Toast.makeText(this, "Login is successful", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                this,
                                "Please verify your email address",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(this, "Invalid email or password", Toast.LENGTH_SHORT).show()
                        return@addOnCompleteListener
                    }
                }
            }

    }
}

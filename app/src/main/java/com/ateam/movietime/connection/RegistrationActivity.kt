package com.ateam.movietime.connection

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Patterns
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ateam.movietime.R
import com.ateam.movietime.util.changeActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        button_reg.setOnClickListener {
            preformRegistration()
        }

        already_have_account_text_view.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        text_view_accept.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/G1IbRujko-A"))
            startActivity(intent)
        }
    }

    private fun preformRegistration() {
        val auth = FirebaseAuth.getInstance()
        val email = email_reg.text.toString()
        val password = password_reg.text.toString()
        val confirmPassword = repassword_reg.text.toString()
        val checkBox = findViewById<CheckBox>(R.id.checkBox)


        if (email.isEmpty()) {
            email_reg.error = "Email cannot be empty"
            return
        }

        if (password.isEmpty()) {
            password_reg.error = "Password cannot be empty"
            return
        }
        if (password.length <= 6) {
            password_reg.error = "The password has to be at least 7 character long"
            return
        }
        if (password != confirmPassword) {
            password_reg.error = "The given passwords are not the same"
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            email_reg.error = "This email address is not valid"
            return
        }
        if (!checkBox.isChecked) {
            Toast.makeText(
                this,
                "You have to accept the General Terms and Conditions.",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = auth.currentUser
                    user?.sendEmailVerification()?.addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                this,
                                "Successfully created a user. Please check email for verification",
                                Toast.LENGTH_SHORT
                            ).show()
                            changeActivity(this, LoginActivity::class.java)
                        } else {
                            Toast.makeText(
                                this,
                                task.exception?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }

    }
}
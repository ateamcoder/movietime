package com.ateam.movietime.api


interface AsyncResponse {
    fun processFinish(output: Any)
}
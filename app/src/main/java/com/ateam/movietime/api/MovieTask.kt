package com.ateam.movietime.api

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.view.View
import android.widget.ProgressBar
import info.movito.themoviedbapi.TmdbApi
import info.movito.themoviedbapi.TmdbMovies


class MovieTask : AsyncTask<Task, Void, Any>() {
    var delegate: AsyncResponse? = null
    @SuppressLint("StaticFieldLeak")
    lateinit var progressBar: ProgressBar
    override fun onPreExecute() {
        progressBar.visibility = View.VISIBLE
    }
    override fun doInBackground(vararg taskDetail: Task) : Any {
        val tmdb = TmdbApi("38d18a7c5ff311c270eda32d7a1d2b35")
        val detail = taskDetail[0]
        return when {
            detail.callType == CallType.SEARCH -> tmdb.search.searchMovie(detail.title, detail.year, detail.language, detail.adult, detail.page)
            detail.callType == CallType.MOVIE -> tmdb.movies.getMovie(detail.movieId, detail.language, TmdbMovies.MovieMethod.videos, TmdbMovies.MovieMethod.credits, TmdbMovies.MovieMethod.similar)
            detail.callType == CallType.NOW_PLAYING -> tmdb.movies.getNowPlayingMovies(detail.language, detail.page, "HU")
            else -> detail
        }
    }

    override fun onPostExecute(result: Any) {
        progressBar.visibility = View.GONE
        delegate!!.processFinish(result)
    }
}
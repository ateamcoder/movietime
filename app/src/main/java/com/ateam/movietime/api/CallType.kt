package com.ateam.movietime.api

enum class CallType {
    SEARCH, MOVIE, NOW_PLAYING
}
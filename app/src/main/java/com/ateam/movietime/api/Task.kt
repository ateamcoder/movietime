package com.ateam.movietime.api

class Task(val callType: CallType, val language: String) {
    var title: String = ""
    var page: Int = 1
    var year: Int? = null
    var adult: Boolean = false
    var movieId: Int = 550
    //detail
    constructor(callType: CallType, movieId: Int, language: String = "en") : this(callType,language) {
        this.movieId = movieId
    }
    //search
    constructor(callType: CallType, title: String, page: Int, year: Int? = null, adult: Boolean = false, language: String = "en") : this(callType,language) {
        this.title = title
        this.year = year
        this.page = page
        this.adult = adult
    }
}
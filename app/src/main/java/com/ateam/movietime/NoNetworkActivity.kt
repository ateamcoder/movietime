package com.ateam.movietime

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ateam.movietime.util.ConnectivityHelper
import com.ateam.movietime.util.changeActivity
import kotlinx.android.synthetic.main.activity_no_network.*
import kotlinx.android.synthetic.main.progress_bar.*


class NoNetworkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_no_network)
        if (ConnectivityHelper.isConnectedToNetwork(this)) {
            changeActivity(this@NoNetworkActivity, MainActivity::class.java)
        }
        button.setOnClickListener {
            val wifi = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            @Suppress("DEPRECATION") //on api level 29+ this always return true
            wifi.isWifiEnabled = true
            progress_include.visibility = View.VISIBLE
            main_progress.visibility = View.VISIBLE
            waitForNetwork()
        }
    }

    private fun waitForNetwork() {
        val t = object : Thread() {
            override fun run() {
                try {
                    while (!ConnectivityHelper.isConnectedToNetwork(this@NoNetworkActivity)) {
                        sleep(1000)
                    }
                    changeActivity(this@NoNetworkActivity, MainActivity::class.java)
                } catch (e: Exception) {

                }
            }
        }
        t.start()
    }
}

package com.ateam.movietime

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.datasource.DataSource
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.ateam.movietime.api.AsyncResponse
import com.ateam.movietime.api.CallType
import com.ateam.movietime.api.MovieTask
import com.ateam.movietime.api.Task
import com.ateam.movietime.model.Movie
import com.ateam.movietime.statistics.StatisticsActivity
import com.ateam.movietime.util.ConnectivityHelper
import com.ateam.movietime.util.changeActivity
import com.ateam.movietime.util.searchViaKeyboard
import com.ateam.movietime.util.setupRecycler
import info.movito.themoviedbapi.model.core.MovieResultsPage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_content.*
import kotlinx.android.synthetic.main.progress_bar.*


class MainActivity : AppCompatActivity(), AsyncResponse {

    private val dataSourceTyped: DataSource<Movie> = emptyDataSourceTyped()
    var page = 1
    var totalPageCount = 1

    override fun processFinish(output: Any) {
        progress_include.visibility = View.GONE
        if (output is MovieResultsPage) {
            Log.d(MainActivity::class.java.simpleName, "total pages: ${output.totalPages}")
            Log.d(MainActivity::class.java.simpleName, "current page: $page")
            totalPageCount = output.totalPages
            output.results.forEach {
                it.posterPath?.let { post ->
                    val poster = "https://image.tmdb.org/t/p/w500$post"
                    dataSourceTyped.add(Movie(it.id, it.title, poster))
                }
            }
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchViaKeyboard(this, search_bar_main, SearchActivity::class.java)
    }

    override fun onStart() {
        val title = intent.getStringExtra(MainActivity::class.java.simpleName)
        if (title != null) {
            search_bar_main.setText(title, TextView.BufferType.EDITABLE)
        }
        setupRecycler(this, dataSourceTyped, main_recycler, main_activity_layout, supportFragmentManager)
        action_bottom_movie.setOnClickListener {
            changeActivity(this, WatchedActivity::class.java)
        }
        action_search.setOnClickListener {
            searchVisibilityChange()
        }
        action_bottom_statistic.setOnClickListener {
            changeActivity(this, StatisticsActivity::class.java)
        }
        main_recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            var isLoading = true
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isLoading && isBottomOfGrid(recyclerView.childCount)) {
                    if (page < totalPageCount) {
                        page++
                        val paging = Task(CallType.NOW_PLAYING, "", page)
                        getNetworkContent(paging)
                    }
                }else {
                    isLoading = false
                }
            }
        })
        super.onStart()
    }

    private fun isBottomOfGrid(childCount: Int): Boolean {
        val totalItemCount = GridLayoutManager(this@MainActivity, 3).itemCount
        val pastVisibleItems = GridLayoutManager(this@MainActivity, 3).findFirstVisibleItemPosition()
        return (childCount + pastVisibleItems) >= totalItemCount
    }

    override fun onResume() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        dataSourceTyped.clear()
        if (!ConnectivityHelper.isConnectedToNetwork(this)) {
            changeActivity(this, NoNetworkActivity::class.java)
        }else {
            val task = Task(CallType.NOW_PLAYING, "", page)
            getNetworkContent(task)
        }
        super.onResume()
    }

    private fun getNetworkContent(task: Task) {
        val mt = MovieTask()
        mt.delegate = this
        mt.progressBar = main_progress
        progress_include.visibility = View.VISIBLE
        mt.execute(task)
    }

    private fun searchVisibilityChange() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val constraintSet = ConstraintSet()
        if (search_bar_layout_main.isVisible) {
            constraintSet.clone(content_layout)
            constraintSet.clear(R.id.category_title, ConstraintSet.TOP)
            constraintSet.applyTo(content_layout)
            action_search.setImageResource(R.drawable.ic_search_black_24dp)
            imm.hideSoftInputFromWindow(search_bar_main.windowToken, 0)
            search_bar_layout_main.visibility = View.INVISIBLE
        } else {
            constraintSet.clone(content_layout)
            constraintSet.clear(R.id.category_title, ConstraintSet.TOP)
            constraintSet.clear(R.id.search_bar_layout_main, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.category_title, ConstraintSet.TOP, R.id.search_bar_layout_main, ConstraintSet.BOTTOM)
            constraintSet.applyTo(content_layout)
            search_bar_layout_main.visibility = View.VISIBLE
            search_bar_main.requestFocus()
            action_search.setImageResource(R.drawable.ic_clearm_black_24dp)
            imm.showSoftInput(search_bar_main, SHOW_IMPLICIT)
        }
    }

    override fun onBackPressed() {
        finishAffinity()
    }
}
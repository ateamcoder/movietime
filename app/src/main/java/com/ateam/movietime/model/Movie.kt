package com.ateam.movietime.model

import info.movito.themoviedbapi.model.Genre

data class Movie(val id: Int, val title: String, val posterPath: String) {
    var release: String? = ""
    var runtime: Int = 0
    var rating: Float = 0.0F
    var imdb: String? = ""
    var description: String? = ""
    var genreList: List<Genre> = emptyList()
    constructor(id: Int, title: String = "", posterPath: String, release: String?, runtime: Int, rating: Float, imdb: String?, description: String?, genreList: List<Genre>) : this(id, title, posterPath) {
        this.release = release
        this.runtime = runtime
        this.rating = rating
        this.imdb = imdb
        this.description = description
        this.genreList = genreList
    }
}
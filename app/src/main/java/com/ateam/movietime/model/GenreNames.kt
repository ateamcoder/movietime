package com.ateam.movietime.model

class GenreNames constructor() {
    var genreNameList = mutableListOf<String>()

    constructor(genreNameList: List<String>) : this() {
        this.genreNameList = genreNameList.toMutableList()
    }
}

package com.ateam.movietime

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.recyclical.datasource.DataSource
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.ateam.movietime.model.GenreNames
import com.ateam.movietime.model.Movie
import com.ateam.movietime.util.changeActivity
import com.ateam.movietime.util.setupRecycler
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.whiteelephant.monthpicker.MonthPickerDialog
import kotlinx.android.synthetic.main.activity_watched.*
import java.util.*
import kotlin.collections.ArrayList


class WatchedActivity : AppCompatActivity() {

    private val dataSourceTyped: DataSource<Movie> = emptyDataSourceTyped()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_watched)
        val uid =  FirebaseAuth.getInstance().currentUser?.uid
        if (uid != null) {
            Log.d(this::class.java.simpleName, uid)
            getAllWatched(uid)
        }else {
            Log.d(this::class.java.simpleName, "null uid")
        }
        searchWithKeyboard(uid)
    }

    override fun onStart() {
        year_filter.setOnClickListener {
            Log.d("year", "click")
            openYearPicker()
        }
        super.onStart()
    }

    override fun onResume() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setupRecycler(this, dataSourceTyped, recycler, watched_activity_layout, supportFragmentManager)
        super.onResume()
    }

    private fun getAllWatched(uid: String) {
        val db = FirebaseFirestore.getInstance()
        val mainDocument = db.collection("Movies").document(uid)
        val userWatched = mainDocument.collection("watched").get()
        userWatched.addOnSuccessListener { movies ->
            if (movies != null) {
                movies.documents.forEach {
                    dataSourceTyped.add(Movie(it.id.toInt(),it.getString("title")!!, it.getString("posterPath")!!))
                }
            }
            else {
                Log.d(this::class.java.simpleName, "No such document")
            }
        }.addOnFailureListener { exception ->
            Log.d(this::class.java.simpleName, "get failed with ", exception)
        }
    }

    private fun openYearPicker() {
        var yearFilter = Calendar.getInstance().get(Calendar.YEAR)
        val builder = MonthPickerDialog.Builder(this@WatchedActivity,
            MonthPickerDialog.OnDateSetListener { _, selectedYear ->
                yearFilter = selectedYear
            }, Calendar.getInstance().get(Calendar.YEAR), 0
        )
        builder
            .showYearOnly()
            .setYearRange(1888, Calendar.getInstance().get(Calendar.YEAR))
            .build()
            .show()
        //TODO MAKE THIS SHIT WORKING
    }

    @SuppressLint("DefaultLocale")
    private fun searchWithKeyboard(uid: String?) {
        search_bar.setOnEditorActionListener(TextView.OnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                val sTitle = search_bar.text.toString()
                val movieList = ArrayList<Movie>()
                if (sTitle.length > 1) {
                    if (movieList.isNotEmpty()) {
                        movieList.clear()
                    }
                    dataSourceTyped.forEach {
                        if (it.title.toLowerCase().contains(sTitle.toLowerCase())) {
                            movieList.add(it)
                        }
                    }
                    dataSourceTyped.clear()
                    dataSourceTyped.addAll(movieList)
                }else {
                    if (uid != null) {
                        dataSourceTyped.clear()
                        getAllWatched(uid)
                    }
                }
                return@OnEditorActionListener true
            } else {
                return@OnEditorActionListener false
            }
        })
    }


    override fun onBackPressed() {
        changeActivity(this, MainActivity::class.java)
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var db = FirebaseFirestore.getInstance()
        private val uid =  FirebaseAuth.getInstance().currentUser?.uid
        fun addMovie(movie: Movie) {
            if (uid != null) {
                val genresNameList = ArrayList<String>()
                movie.genreList.forEach { genresNameList.add(it.name) }
                val uidDoc = db.collection("Movies").document(uid)
                uidDoc.collection("watched").document(movie.id.toString()).set(movie)
                uidDoc.collection("genres").document(movie.id.toString()).set(GenreNames(genresNameList))
            }
        }
        fun deleteMovie(movie: Movie) {
            if (uid != null) {
                val uidDoc = db.collection("Movies").document(uid)
                uidDoc.collection("watched").document(movie.id.toString()).delete()
                uidDoc.collection("genres").document(movie.id.toString()).delete()
            }
        }
    }
}

package com.ateam.movietime

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.datasource.DataSource
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.ateam.movietime.api.AsyncResponse
import com.ateam.movietime.api.CallType
import com.ateam.movietime.api.MovieTask
import com.ateam.movietime.api.Task
import com.ateam.movietime.model.Movie
import com.ateam.movietime.util.changeActivity
import com.ateam.movietime.util.setupRecycler
import com.whiteelephant.monthpicker.MonthPickerDialog
import info.movito.themoviedbapi.model.core.MovieResultsPage
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.progress_bar.*
import java.util.*


class SearchActivity : AppCompatActivity(), AsyncResponse {

    private val dataSourceTyped: DataSource<Movie> = emptyDataSourceTyped()
    var page = 1
    var totalPageCount = 1
    private var year: Int? = null

    override fun processFinish(output: Any) {
        progress_include.visibility = View.GONE
        if (output is MovieResultsPage) {
            Log.d(SearchActivity::class.java.simpleName, "total pages: ${output.totalPages}")
            totalPageCount = output.totalPages
            output.results.forEach {
                if (it.posterPath != null) {
                    val poster = "https://image.tmdb.org/t/p/w500${it.posterPath}"
                    dataSourceTyped += Movie(it.id, it.title, poster)
                    Log.d(SearchActivity::class.java.simpleName, it.toString())
                }else{
                    Log.d(SearchActivity::class.java.simpleName, "${it.title} --- null poster path")
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        val title = intent.getStringExtra(SearchActivity::class.java.simpleName)
        if (title != null) {
            val task = Task(CallType.SEARCH, title, page)
            getNetworkContent(task)
        }

        search_bar.setText(title, TextView.BufferType.EDITABLE)
        searchWithKeyboard()
        search_recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            var isLoading = true
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isLoading && isBottomOfGrid(recyclerView.childCount)) {
                    if (page < totalPageCount) {
                        page++
                        val paging = Task(CallType.SEARCH, search_bar.text.toString(), page)
                        getNetworkContent(paging)
                    }
                    else {
                        isLoading = false
                    }
                }
            }
        })
        year_filter.setOnClickListener {
            openYearPicker()
        }
    }

    override fun onResume() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setupRecycler(this, dataSourceTyped, search_recycler, search_activity_layout, supportFragmentManager)
        super.onResume()
    }

    private fun openYearPicker() {
        val builder = MonthPickerDialog.Builder(this@SearchActivity,
            MonthPickerDialog.OnDateSetListener { _, selectedYear ->
                year = selectedYear
                Log.d(this::class.java.simpleName, "INSIDE LOG")
                val task = Task(CallType.SEARCH, search_bar.text.toString(), 1, year = selectedYear)
                getNetworkContent(task, true)
            }, Calendar.getInstance().get(Calendar.YEAR), 0
        )
        builder
            .showYearOnly()
            .setYearRange(1888, Calendar.getInstance().get(Calendar.YEAR) + 6)
            .build()
            .show()
    }

    private fun searchWithKeyboard() {
        search_bar.setOnEditorActionListener(TextView.OnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                val title = search_bar.text.toString()
                if (title.length > 1) {
                    page = 1
                    val task = Task(CallType.SEARCH, title, page, year = null)
                    getNetworkContent(task, true)
                }
                return@OnEditorActionListener true
            } else {
                return@OnEditorActionListener false
            }
        })
    }

    private fun getNetworkContent(task: Task, clearDataSource: Boolean = false) {
        val mt = MovieTask()
        mt.delegate = this
        mt.progressBar = main_progress
        progress_include.visibility = View.VISIBLE
        if (clearDataSource) dataSourceTyped.clear()
        mt.execute(task)
    }

    private fun isBottomOfGrid(childCount: Int): Boolean {
        val totalItemCount = GridLayoutManager(this@SearchActivity, 3).itemCount
        val pastVisibleItems = GridLayoutManager(this@SearchActivity, 3).findFirstVisibleItemPosition()
        return (childCount + pastVisibleItems) >= totalItemCount
    }

    override fun onBackPressed() {
        changeActivity(this, MainActivity::class.java, search_bar.text.toString())
    }
}

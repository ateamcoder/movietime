package com.ateam.movietime.util

import android.content.pm.ActivityInfo
import android.view.View
import android.view.View.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.children
import com.ateam.movietime.MovieDetailDialog
import kotlinx.android.synthetic.main.dialog_movie_content.*
import kotlinx.android.synthetic.main.dialog_movie_detail.*


class FullScreenHelper {

    fun enterFullScreen(movieDetailDialog: MovieDetailDialog) {
        val newLayoutParams = movieDetailDialog.youtube_player_view.layoutParams as ConstraintLayout.LayoutParams
        newLayoutParams.marginStart = 0
        newLayoutParams.marginEnd = 0
        newLayoutParams.bottomMargin = 0
        newLayoutParams.topMargin = 0
        movieDetailDialog.youtube_player_view.layoutParams = newLayoutParams
        movieDetailDialog.activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        val decorView =  movieDetailDialog.activity?.window?.decorView
        if (decorView != null) {
            hideSystemUi(decorView)
            movieDetailDialog.toolbar.visibility = GONE
            movieDetailDialog.movie_content_layout.children.forEach {
                if (it.id != movieDetailDialog.youtube_player_view.id) {
                    it.visibility = GONE
                    it.invalidate()
                }
            }
        }
    }

    fun exitFullScreen(movieDetailDialog: MovieDetailDialog) {
        val newLayoutParams = movieDetailDialog.youtube_player_view.layoutParams as ConstraintLayout.LayoutParams
        newLayoutParams.marginStart = 8
        newLayoutParams.marginEnd = 8
        newLayoutParams.bottomMargin = 8
        newLayoutParams.topMargin = 16
        movieDetailDialog.youtube_player_view.layoutParams = newLayoutParams
        movieDetailDialog.activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val decorView = movieDetailDialog.activity?.window?.decorView
        if (decorView != null) {
            showSystemUi(decorView)
            movieDetailDialog.toolbar.visibility = VISIBLE
            movieDetailDialog.movie_content_layout.children.forEach {
                if (it.id != movieDetailDialog.youtube_player_view.id) {
                    it.visibility = VISIBLE
                    it.invalidate()
                }
            }
        }
    }

    private fun hideSystemUi(mDecorView: View) {
        mDecorView.systemUiVisibility = (SYSTEM_UI_FLAG_LAYOUT_STABLE
                or SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or SYSTEM_UI_FLAG_FULLSCREEN
                or SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    private fun showSystemUi(mDecorView: View) {
        mDecorView.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE
    }
}
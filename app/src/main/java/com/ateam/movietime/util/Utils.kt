package com.ateam.movietime.util

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.datasource.DataSource
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.withItem
import com.ateam.movietime.MainActivity
import com.ateam.movietime.MovieDetailDialog
import com.ateam.movietime.R
import com.ateam.movietime.WatchedActivity
import com.ateam.movietime.holder.MovieViewHolder
import com.ateam.movietime.model.Movie
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_main_content.view.*

fun setupRecycler(context: Context, dataSourceTyped: DataSource<Movie>, recycler: RecyclerView, view: View, manager: FragmentManager) {
    recycler.setup {
        withDataSource(dataSourceTyped)
        withLayoutManager(GridLayoutManager(context, 3))
        withItem<Movie, MovieViewHolder>(R.layout.activity_main_grid_holder) {
            onBind(::MovieViewHolder) { _, item ->
                Glide.with(context)
                    .load(item.posterPath)
                    .placeholder(ColorDrawable(Color.LTGRAY))
                    .into(image)
            }
            onClick {
                Snackbar.make(view, item.title, Snackbar.LENGTH_SHORT).let {
                    if (context is MainActivity)
                        it.setAnchorView(view.action_search).show()
                    else
                        it.show()
                }
                if(context is WatchedActivity) {
                    val movieDetailDialog = MovieDetailDialog(item.id, true, dataSourceTyped)
                    movieDetailDialog.display(manager)
                }else {
                    val movieDetailDialog = MovieDetailDialog(item.id, dataSource = dataSourceTyped)
                    movieDetailDialog.display(manager)
                }
            }
        }
        withEmptyView(view.no_data)
    }
}

fun <T : AppCompatActivity> searchViaKeyboard(context: Context, search_bar: TextInputEditText, nextActivity: Class<T>) {
    search_bar.setOnEditorActionListener(TextView.OnEditorActionListener { _, i, _ ->
        if (i == EditorInfo.IME_ACTION_SEARCH) {
            if (search_bar.text!!.length > 1) {
                changeActivity(context, nextActivity, search_bar.text.toString())
            }
            return@OnEditorActionListener true
        } else {
            return@OnEditorActionListener false
        }
    })
}

fun <T : AppCompatActivity> changeActivity(context: Context, nextActivity: Class<T>, message: String? = null) {
    val intent = Intent(context, nextActivity).apply {
        if (message != null) {
            putExtra(nextActivity.simpleName, message)
        }
    }
    startActivity(context, intent, null)
}
package com.ateam.movietime.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.afollestad.recyclical.ViewHolder
import com.ateam.movietime.R

class CastViewHolder(itemView: View) : ViewHolder(itemView) {
    val castImageView = itemView.findViewById<ImageView>(R.id.cast_image_View)
    val characterNameView = itemView.findViewById<TextView>(R.id.character_name)
    val realNameView = itemView.findViewById<TextView>(R.id.real_name)
}
package com.ateam.movietime.holder

import android.view.View
import android.widget.ImageView
import com.afollestad.recyclical.ViewHolder
import com.ateam.movietime.R

class MovieViewHolder(itemView: View) : ViewHolder(itemView) {
    val image: ImageView = itemView.findViewById(R.id.recycler_image)
}
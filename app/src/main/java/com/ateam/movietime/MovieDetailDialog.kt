package com.ateam.movietime

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.recyclical.datasource.DataSource
import com.afollestad.recyclical.datasource.dataSourceOf
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.withItem
import com.ateam.movietime.api.AsyncResponse
import com.ateam.movietime.api.CallType
import com.ateam.movietime.api.MovieTask
import com.ateam.movietime.api.Task
import com.ateam.movietime.holder.CastViewHolder
import com.ateam.movietime.holder.MovieViewHolder
import com.ateam.movietime.model.Movie
import com.ateam.movietime.util.FullScreenHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.google.android.material.snackbar.Snackbar
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import info.movito.themoviedbapi.model.MovieDb
import info.movito.themoviedbapi.model.Video
import info.movito.themoviedbapi.model.people.PersonCast
import kotlinx.android.synthetic.main.dialog_movie_content.*
import kotlinx.android.synthetic.main.progress_bar.*


class MovieDetailDialog(
    private val movieId: Int,
    private val isWatched: Boolean = false,
    var dataSource: DataSource<Movie>
) : DialogFragment(), AsyncResponse {

    private lateinit var movie: Movie
    private lateinit var toolbar: Toolbar
    private lateinit var youTubePlayerView: YouTubePlayerView
    private val fullScreenHelper = FullScreenHelper()
    private lateinit var progressView: View

    override fun processFinish(output: Any) {
        progressView.visibility = View.GONE
        if (output is MovieDb) {
            val title = output.title
            val imdb = "https://www.imdb.com/title/${output.imdbID}"
            val description = output.overview
            val release = output.releaseDate
            val runtime = output.runtime
            val rating = output.voteAverage
            val posterPath = "https://image.tmdb.org/t/p/w500${output.posterPath}"
            val genreList = output.genres
            movie = Movie(movieId, title, posterPath, release, runtime, rating, imdb, description, genreList)

            setupViewItemsData()
            setupViewVideoData(output.videos)
            setupViewCastData(output.cast)
            setupViewSimilarData(output.similarMovies)
        }

        if (output is Task) {
            Log.d(this::class.java.simpleName, "no video or artwork")
        }
    }

    private fun setupViewSimilarData(similarMovies: List<MovieDb>) {
        if (similarMovies.isNotEmpty()) {
            textView9.visibility = View.VISIBLE
            val similarDataSource = emptyDataSourceTyped<Movie>()
            similarMovies.forEach {
                similarDataSource.add(Movie(it.id, it.title, "https://image.tmdb.org/t/p/w500${it.posterPath}"))
            }
            similar_recycler.setup {
                withDataSource(similarDataSource)
                withLayoutManager(LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
                withItem<Movie, MovieViewHolder>(R.layout.activity_main_grid_holder) {
                    onBind(::MovieViewHolder) { _, item ->
                        Glide.with(this@MovieDetailDialog)
                            .load(item.posterPath)
                            .placeholder(ColorDrawable(Color.LTGRAY))
                            .into(image)
                    }
                    onClick {
                        Log.d("click", "title: ${item.title}")
                        val movieDetailDialog = MovieDetailDialog(item.id, dataSource = similarDataSource)
                        if (fragmentManager != null) {
                            movieDetailDialog.display(fragmentManager!!)
                        }
                    }
                }
            }
        }
    }

    private fun setupViewVideoData(videoList: List<Video>) {
        if (videoList.isNotEmpty()) {
            val site = videoList[0].site
            val key = videoList[0].key
            if (site == "YouTube") {
                youTubePlayerView.visibility = View.VISIBLE
                lifecycle.addObserver(youTubePlayerView)
                youTubePlayerView.addYouTubePlayerListener(object :
                    AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        youTubePlayer.cueVideo(key, 0f)
                    }
                })
                youTubePlayerView.addFullScreenListener(object : YouTubePlayerFullScreenListener {
                    override fun onYouTubePlayerEnterFullScreen() {
                        fullScreenHelper.enterFullScreen(this@MovieDetailDialog)
                    }

                    override fun onYouTubePlayerExitFullScreen() {
                        fullScreenHelper.exitFullScreen(this@MovieDetailDialog)
                    }
                })
            }
        }
    }

    private fun setupViewCastData(castList: MutableList<PersonCast>) {
        textView8.movementMethod = LinkMovementMethod.getInstance()
        textView8.setOnClickListener {
            val castLink = "${movie.imdb}/fullcredits?ref_=tt_cl_sm#cast"
            val linkIntent = Intent(Intent.ACTION_VIEW)
            linkIntent.data = Uri.parse(castLink)
            startActivity(linkIntent)
        }
        if (castList.isNotEmpty()) {
            val dataSourceCast = dataSourceOf()
            if (castList.size >= 8) {
                for (i in 0..8) {
                    dataSourceCast.add(castList[i])
                }
            } else {
                for (i in 0 until castList.size) {
                    dataSourceCast.add(castList[i])
                }
            }
            setupCastRecyclerView(dataSourceCast)
        }
    }

    private fun setupCastRecyclerView(dataSourceCast: DataSource<Any>) {
        cast_recycler.setup {
            withDataSource(dataSourceCast)
            withLayoutManager(LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
            withItem<PersonCast, CastViewHolder>(R.layout.cast_view) {
                onBind(::CastViewHolder) { _, item ->
                    val path = "https://image.tmdb.org/t/p/w185${item.profilePath}"
                    Glide.with(this@MovieDetailDialog)
                        .load(path)
                        .placeholder(R.drawable.ic_no_profile_image)
                        .override(90, 120)
                        .transform(CircleCrop())
                        .into(castImageView)
                    characterNameView.text = item.character
                    realNameView.text = item.name
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupViewItemsData() {
        with(movie) {
            toolbar.title = title
            title_header.text = title
            year.text = release
            run_time.text = "$runtime min"

            val fiveStar = rating / 2
            rating_bar.rating = fiveStar

            var genresText = ""
            genreList.forEach {
                genresText += if (it != genreList.last()) {
                    it.name + ", "
                } else {
                    it.name
                }

            }
            genres_name.text = genresText
            overview.text = description
        }
    }

    fun display(fragmentManager: FragmentManager): MovieDetailDialog {
        val movieDetailDialog = MovieDetailDialog(movieId, isWatched, dataSource)
        movieDetailDialog.show(fragmentManager, "dialog_movie_detail_add")
        return movieDetailDialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
            dialog.window?.setLayout(width, height)
            dialog.window?.setWindowAnimations(R.style.AppTheme_Slide)
            youTubePlayerView = dialog.youtube_player_view
            progressView = dialog.progress_include
        }
    }

    override fun onResume() {
        val mt = MovieTask()
        val task = Task(CallType.MOVIE, movieId)
        getNetworkContent(task, mt)
        super.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_movie_detail, container, false)
        toolbar = view.findViewById(R.id.toolbar)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { dismiss() }
        if (!isWatched) {
            toolbar.inflateMenu(R.menu.dialog_movie_detail_add)
        } else {
            toolbar.inflateMenu(R.menu.dialog_movie_detail_remove)
        }
        toolbar.setOnMenuItemClickListener {
            if (!isWatched) {
                WatchedActivity.addMovie(movie)
                Snackbar.make(view, "Movie added to your watched list", Snackbar.LENGTH_SHORT)
                    .show()
            } else {
                WatchedActivity.deleteMovie(movie)
                dataSource.remove(movie)
                Snackbar.make(view, "Movie deleted to your watched list", Snackbar.LENGTH_SHORT)
                    .show()
            }
            dismiss()
            return@setOnMenuItemClickListener true
        }
    }

    private fun getNetworkContent(task: Task, mt: MovieTask) {
        mt.delegate = this
        mt.progressBar = main_progress
        progress_include.visibility = View.VISIBLE
        mt.execute(task)
    }

    override fun onDestroy() {

        youTubePlayerView.release()
        super.onDestroy()
    }
}

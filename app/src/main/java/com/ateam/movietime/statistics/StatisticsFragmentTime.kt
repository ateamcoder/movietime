package com.ateam.movietime.statistics


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ateam.movietime.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_statistics_time.*


class StatisticsFragmentTime : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_statistics_time, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val db = FirebaseFirestore.getInstance()
        val uid =  FirebaseAuth.getInstance().currentUser?.uid

        if (uid != null) {
            val mainDocument = db.collection("Movies").document(uid)
            val userWatched = mainDocument.collection("watched").get()
            userWatched.addOnCompleteListener { movies ->
                val count = movies.result?.documents?.size
                piece.text = count.toString()
                var minutes = 0L
                movies.result?.documents?.forEach {
                    minutes += it.getLong("runtime")!!
                }

                val months = (minutes / 43829.0639)
                val days = (minutes / 1440 % 30.44)
                val hours = minutes / 60 % 24
                monthView.text = months.toInt().toString()
                hourView.text = hours.toInt().toString()
                dayView.text = days.toInt().toString()
            }
        }
    }
}

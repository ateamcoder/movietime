package com.ateam.movietime.statistics


import android.graphics.Color.GREEN
import android.graphics.Color.WHITE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ateam.movietime.R
import com.ateam.movietime.model.GenreNames
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot


class StatisticsFragmentPie : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_statistics_fragment_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val pieChart = view.findViewById<PieChart>(R.id.piechart)
        pieChart.setNoDataText("")
        val db = FirebaseFirestore.getInstance()
        val uid = FirebaseAuth.getInstance().currentUser?.uid

        if (uid != null) {
            val userGenres = db.collection("Movies").document(uid).collection("genres").get()
            val genreMap = HashMap<String, Int>()
            userGenres.addOnCompleteListener { movies ->
                if (movies.result?.documents?.size != 0) {
                    setUpPieChart(pieChart)
                    getGenreMap(movies, genreMap)
                    pieChart.data = setupPieData(genreMap)
                }else {
                    Snackbar.make(view, R.string.no_data_pie, Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun setupPieData(genreMap: HashMap<String, Int>): PieData {
        val dataSet = PieDataSet(getPieEntryList(genreMap), "")
        dataSet.sliceSpace = 3F
        dataSet.selectionShift = 5F
        dataSet.colors = getColorsList()

        val data = PieData(dataSet)
        data.setValueTextSize(10F)
        data.setValueTextColor(GREEN)
        data.isHighlightEnabled
        return data
    }

    private fun getPieEntryList(genreMap: HashMap<String, Int>): ArrayList<PieEntry> {
        val pieEntries = ArrayList<PieEntry>()

        if (genreMap.size >= 5) {
            var countAllGenre = 0
            var countTopGenre = 0
            genreMap.values.forEach { countAllGenre += it }
            genreMap.entries.sortedByDescending { it.value }.subList(0, 4).forEach {
                countTopGenre += it.value
                pieEntries.add(PieEntry(it.value.toFloat(), it.key))
            }
            pieEntries.add(PieEntry((countAllGenre - countTopGenre).toFloat(), "Other"))
        }else {
            genreMap.entries.forEach {
                pieEntries.add(PieEntry(it.value.toFloat(), it.key))
            }
        }
        return pieEntries
    }

    private fun getColorsList(): ArrayList<Int> {
        val colors = ArrayList<Int>()

        for (c in ColorTemplate.COLORFUL_COLORS) {
            colors.add(c)
        }

        for (c in ColorTemplate.VORDIPLOM_COLORS) {
            colors.add(c)
        }

        for (c in ColorTemplate.JOYFUL_COLORS) {
            colors.add(c)
        }
        return colors
    }

    private fun getGenreMap(
        movies: Task<QuerySnapshot>,
        genreMap: HashMap<String, Int>
    ) {
        movies.result?.documents?.forEach {
            it.toObject(GenreNames::class.java)!!.genreNameList.forEach { genre ->
                val j = genreMap[genre]
                genreMap[genre] = if (j == null) 1 else j + 1
            }
        }
    }

    private fun setUpPieChart(pieChart: PieChart) {
        pieChart.centerText = "Top genres"
        pieChart.setUsePercentValues(true)
        pieChart.description.isEnabled = false
        pieChart.setExtraOffsets(4F, 10F, 5F, 5F)
        pieChart.dragDecelerationFrictionCoef = 0.45F
        pieChart.isRotationEnabled = false
        pieChart.isDrawHoleEnabled = true
        pieChart.setHoleColor(WHITE)
        pieChart.transparentCircleRadius = 58F
        pieChart.animateY(1500, Easing.EaseInOutCubic)
    }
}
